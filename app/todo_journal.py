"""class for working with todo file"""
import sys
import json
import os.path

class TodoJournal:
    """Class with todo"""

    def __init__(self, path):
        """initialisation of arguments etc"""
        self.path_todo = path
        self.create(self.path_todo, 'default')
        self.name = self._parse()["name"]
        self.entries = self._parse()["todos"]
    @staticmethod
    def create(filename, name):
        """creation of json file for todos"""

        with open(filename, "w", encoding='utf-8') as todo_file:
            json.dump(
                {"name": name, "todos": []},
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def add_todo(self, new_todo):
        """Adding new todo to todo-file
        todo - text of todo to add"""
        data = self._parse()

        name = data["name"]
        todos = data["todos"]
        obj = iter(todos)
        type(todos)
        if todos == []:
            todos.append(new_todo)
        else:
            for i in range(len(todos)):
                if next(obj) == new_todo:
                    raise Exception(f'todo already exists')
            else:
                todos.append(new_todo)
        new_data = \
        {
            "name": name,
            "todos": todos,
        }
        self.entries = todos
        self._update(new_data)

    def __iter__(self):
        """iterator"""
        return self
    def __next__(self):
        """magick method next"""
        value = self.entries
        end = 0
        if end < len(self.entries):
            self.entries += 1
            end += 1
            return value
        else:
            raise StopIteration
    def __getitem__(self, index):
        """method for getting item by its index"""
        data = self._parse()
        todos = data['todos']
        return "Number " + str(index) + ": " + str(todos)

    def remove_todo(self, index):
        """Removing todo by its index
        index - number of todo to delete, starting with 0"""
        data = self._parse()
        name = data["name"]
        todos = data["todos"]

        todos.remove(todos[index])

        new_data = {
            "name": name,
            "todos": todos,
        }

        self._update(new_data)

    def __len__(self):
        """magick method len"""
        return len(self.entries)

    def _update(self, new_data):
        """updating file for seeing new and deleted todos"""
        with open(self.path_todo, "w", encoding='utf-8') as todo_file:
            json.dump(
                new_data,
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def _parse(self):
        """Reading from self.path and returning json with todos
        :returns json with todos
        """

        try:
            with open(self.path_todo, 'r', encoding="utf-8") as todo_file:
                data = json.load(todo_file)
                name = data["name"]
                return data
        except FileNotFoundError as err:
            print(f"{err}")
            print(f"Не существует такой тудушки: {self.path_todo}")
            sys.exit(1)

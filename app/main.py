from fastapi import FastAPI,Query
from pydantic import BaseModel, Field
from typing import Union
from enum import Enum
from app.todo_journal import TodoJournal
import os.path

class info(Enum):
    user13 = "Leushkin"
    user14 = "Lukyanow"
    user15 = "Mitrohin"
    me = "me"

class Todo(BaseModel):
    name: str = Field(default="Put here smth",example="Some name of some todo")
    todos: list | None = Field(default=None, example=("task1","task2"))

class Data(BaseModel):
    title: str | None = Query(default = None, min_length=3,max_length=50)
    todo: Todo | None = None

class DataOut(Data):
    pass

class DataIn(Data):
    path: str = Query(default = None, max_length=15)
    existance: bool

class Config:
    schema_extra = {
        "title": {
            "name": "default",
            "todos": "None"
        }
    }


app = FastAPI()


@app.get("/")
def root():
    return {"message": "Hello World"}

@app.get("/chat/{num}")
def sec(num: int, q: Union[str, None] = None):
    if q == "secret":
        return {"Secret Chat " + "number: " + f"{num}"}
    return {"num": num}

@app.get("/hello/{name}")
def say_hello(name: str):
    return {"message": f"Hello {name}"}

@app.get("/users/{item_id}")
def create_item(item_id: info, skip: int = 0, limit: int = 25):
    if item_id is info.me:
        return{"item_id": item_id}
    if item_id is info.user13:
        return{"Username": info.user13.value}
    if item_id is info.user14:
        return{"Username": info.user14.value}
    if item_id is info.user15:
        return{"Username": info.user15.value}
    return{"item_id": item_id}

@app.get("/items/show")
def read_items(skip: int = 1, limit: int = 10):
    items = list(range(0,100))
    value = 1
    for item in items:
        items[item] += value
    return items[(skip-1)*limit:skip*limit:1]

@app.put("/items/{item_id}")
def create_item(item_id: int, item: Data):
    return{"item_id": item_id,"title": item.title,"name": item.name, "item_todos": item.todos}

@app.post("/items/")
def create_new_item(item: Data):
    return item

@app.post("/items/show/{command}")
def unit(command: int, item: Data, skip: int = 1, limit: int = 10):
    return{"command": command, "name": item.name, "todos": item.todos[(skip-1)*limit:skip*limit:1]}

@app.get("/todo")
def  get_todo(item: Data):
    return{"title": item.title,"name": item.name, "item_todos": item.todos}

@app.put("/todo/create/{title}")
def todo_create(item: Data, title: str = Query(default = "NoName", min_length=3,max_length=50),name: str | None = Query(default="Put smth here")):
    todo = TodoJournal("info.txt")
    todo.create("info.txt",name)
    item.name = name
    item.title = title
    return {"title": title, "name": todo.name}

@app.put("/todo/add_todo")
def todo_add(task: str = Query(default="Nothing to do :0")):
    todo = TodoJournal(todo_create())
    todo.add_todo(task)
    return {}

def path_checker(path: str):
    if (os.path.exists(path)):
        return True

@app.post("/todo/response", response_model=DataOut)
def create_todo(todo: DataIn):
    todo.existance = path_checker(todo.path)
    return todo

@app.get("/todos/", response_model=list[DataOut])
def show():
    todos = [{},{},{}]
    return todos
